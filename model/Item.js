const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PetSchema = new Schema({
    petName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true,
        default: Date.now()
    }
});  

module.exports = Pet = mongoose.model('pet', PetSchema); 