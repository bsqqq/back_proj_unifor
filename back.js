const express = require('express');
const helmet = require('helmet')
const cors = require('cors');
const { MongoClient } = require('mongodb');
const db = 'nao vou moscar minha URI string aqui ta maluco kkkk';

const corsOptions = {
    origin: '*',
    methods: "GET, HEAD, POST, PATCH, PUT, DELETE",
    preflightsContinue: false,
    opitionSuccessStatus: 200
}
const app = express();
app.use(
    express.urlencoded({ extended: true }),
    express.json(),
    cors(corsOptions),
    helmet(),
)

app.get('/', cors(), (_, res) => {
    MongoClient.connect(db, { useUnifiedTopology: true }, (err, datab) => {
        if (err) throw err;
        datab.db('unifor').collection('projeto').find().toArray((err, result) => {
            if (err) throw err;
            res.send(result);
            datab.close()
        })
    })
  }
)

app.post('/post', cors(), (req, res) => {
    const { petName, petPrice, petDate, petDesc } = req.body
    console.log(`Dados recebidos! Nome: ${petName}, Preço: ${petPrice}, Data: ${petDate}R$›, Descrição: ${petDesc}`)
    MongoClient.connect(db, { useUnifiedTopology: true }, (err, datab) => {
        if (err) throw err;
        var dbo = datab.db("unifor");
        var myobj = { 
            Pet_Name: petName, 
            Price: petPrice, 
            Date: petDate, 
            Description: petDesc };
        dbo.collection("projeto").insertOne(myobj, (err, res) => {
          if (err) throw err;
          console.log("1 documento inserido com sucesso");
          datab.close();
        });
        res.send('Dados recebidos com sucesso')
      }); 
    })

app.listen(3333, () => {
    console.log("Escutando na porta 3333.");
})
